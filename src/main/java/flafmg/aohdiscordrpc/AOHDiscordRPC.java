package flafmg.aohdiscordrpc;

import age.of.civilizations2.jakowski.lukasz.*;
import age.of.modifications.flafmg.mod.Mod;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class AOHDiscordRPC extends Mod {
    private DiscordRPCManager discordRPCManager;
    private ScheduledExecutorService scheduler;

    @Override
    public void onInit() {
        discordRPCManager = new DiscordRPCManager();
        discordRPCManager.init();

        scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(this::updateDiscordStatus, 0, 1, TimeUnit.SECONDS);
    }

    @Override
    public void onDisable() {
        if (scheduler != null && !scheduler.isShutdown()) {
            scheduler.shutdown();
        }
        discordRPCManager.shutdown();
    }

    private void updateDiscordStatus() {
        String civName = CFG.game.getCiv(CFG.game.getActiveCivID()).civGameData.sCivName;
        int turnCount = Game_Calendar.TURN_ID;

        if(CFG.game.getCiv(CFG.game.getActiveCivID()).civGameData.sCivName == CFG.langManager.getCiv("neu")) {
            discordRPCManager.updateOnMenu();
        }else {
            discordRPCManager.updateOnGame(civName, turnCount);
        }
        discordRPCManager.runCallbacks();
    }
}
