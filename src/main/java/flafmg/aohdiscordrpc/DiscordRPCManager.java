package flafmg.aohdiscordrpc;

import net.arikia.dev.drpc.DiscordEventHandlers;
import net.arikia.dev.drpc.DiscordRichPresence;
import net.arikia.dev.drpc.DiscordRPC;
import net.arikia.dev.drpc.DiscordUser;

public class DiscordRPCManager {
    private final String appId = "1246321373106671627";
    private long startTime;

    public void init() {
        DiscordEventHandlers handlers = new DiscordEventHandlers.Builder()
                .setReadyEventHandler((DiscordUser user) -> System.out.println("If youre seeing this, discord rpc is working!"))
                .build();
        DiscordRPC.discordInitialize(appId, handlers, true);

        startTime = System.currentTimeMillis() / 1000L;

        updateOnMenu();
    }

    public void updateOnMenu() {
        DiscordRichPresence presence = new DiscordRichPresence.Builder("")
                .setDetails("In the Main Menu")
                .setBigImage("aoh2_logo", "Age of History II : Menu")
                .setStartTimestamps(startTime)
                .build();
        DiscordRPC.discordUpdatePresence(presence);
    }

    public void updateOnGame(String civilizationName, int turnCount) {
        DiscordRichPresence presence = new DiscordRichPresence.Builder("Turn: " + turnCount)
                .setDetails("Civilization: " + civilizationName)
                .setBigImage("aoh2_logo", "Age of History II : in Game")
                .setStartTimestamps(startTime)
                .build();
        DiscordRPC.discordUpdatePresence(presence);
    }

    public void shutdown() {
        DiscordRPC.discordShutdown();
    }

    public void runCallbacks() {
        DiscordRPC.discordRunCallbacks();
    }
}
