# AoH2 Discord RPC

### A simple mod to add Discord RPC to Age of History 2

**For [AoM Modloader](https://gitlab.com/age-of-modifications/age-of-modifications) only**

---

## Download

Get the latest release [here](https://gitlab.com/age-of-modifications/mods/aoh2-discord-rpc/-/releases).

---

## Screenshots

**Menu:**
![Menu](https://i.imgur.com/VWg9Qbo.png)

**In-Game:**
![In Game](https://i.imgur.com/118BfCa.png)

---

<sub>made in brazil with <3</sub>
